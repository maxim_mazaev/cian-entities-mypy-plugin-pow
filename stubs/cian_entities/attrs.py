from typing import Any


class Attr(object):
    creation_counter: int
    python_type: Any = ...

    def __init__(self, required: int, help: str, default: int) -> None: ...

    def __call__(self, method: str) -> Attr: ...


class Integer(Attr):
    def __init__(self, min: int = None, max: int = None, **kwargs) -> None: ...


class String(Attr):
    def __init__(self, min: int = None, max: int = None, **kwargs) -> None: ...


class Boolean(Attr):
    def __init__(self, min: int = None, max: int = None, **kwargs) -> None: ...
