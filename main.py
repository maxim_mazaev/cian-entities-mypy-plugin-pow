from cian_entities import ValueObject, attrs


class Offer(ValueObject):
    offer_id = attrs.Integer(help='')
    name = attrs.String(help='')
    is_ok = attrs.Boolean(help='')


def foo(var: bool) -> bool:
    return var

# todo не работает если не заполнить все поля
offer = Offer(offer_id='232werwerwer', name=2, is_ok='2222223')  # expected mypy error
foo(offer.offer_id)  # expected mypy error
foo(offer.name)  # expected mypy error
foo(offer.is_ok)  # expected ok
