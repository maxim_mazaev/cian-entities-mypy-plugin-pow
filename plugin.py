from mypy.plugin import Plugin
from collections import OrderedDict
from typing import Any, Optional, List, Union

import mypy.plugin  # To avoid circular imports.
from mypy.nodes import (
    Context, Argument, Var, ARG_POS, TypeInfo, AssignmentStmt,
    NameExpr, CallExpr, Expression, JsonDict,
)
from mypy.plugins.common import (
    add_method
)
from mypy.types import Type, NoneTyp, TypeVarDef
from mypy.typevars import fill_typevars
from mypy.util import unmangle


type_mapping = {
    'Integer': 'builtins.int',
    'String': 'builtins.str',
    'Boolean': 'builtins.bool',
}


class Attribute:
    """The value of an attr.ib() call."""

    def __init__(self, name: str, info: TypeInfo,
                 context: Context) -> None:
        self.name = name
        self.info = info
        self.context = context

    def argument(self, ctx: 'mypy.plugin.ClassDefContext') -> Argument:
        """Return this attribute as an argument to __init__."""
        init_type = self.info[self.name].type

        # Attrs removes leading underscores when creating the __init__ arguments.
        return Argument(
            Var(self.name.lstrip("_"), init_type),
            init_type,
            None,
            ARG_POS,
        )

    def serialize(self) -> JsonDict:
        """Serialize this object so it can be saved and restored."""
        return {
            'name': self.name,
            'context_line': self.context.line,
            'context_column': self.context.column,
        }

    @classmethod
    def deserialize(cls, info: TypeInfo, data: JsonDict) -> 'Attribute':
        """Return the Attribute that was serialized."""
        return Attribute(
            data['name'],
            info,
            Context(line=data['context_line'], column=data['context_column'])
        )


def get_native_typed_attribute(
        ctx: 'mypy.plugin.ClassDefContext',
        lhs: Union[Expression, NameExpr],
        rvalue: Union[Expression, CallExpr],
        stmt: AssignmentStmt
) -> Optional[Any]:
    """
    Возвращаем атрибут с подменным нативным типом
    """
    source_type_name = rvalue.callee.node.name()
    target_type_name = type_mapping[source_type_name]
    inferred_type = ctx.api.named_type_or_none(target_type_name)
    lhs.node.type = inferred_type
    lhs.is_inferred_def = False

    # FIXME тупой хак для подмены типа экспрешена
    expr_data = rvalue.callee.node.serialize()
    expr_data['defn']['name'] = target_type_name
    rvalue.callee.node = TypeInfo.deserialize(expr_data)
    # --------------------------------------------- #

    name = unmangle(lhs.name)
    return Attribute(name, ctx.cls.info, stmt)


def _analyze_class(
        ctx: 'mypy.plugin.ClassDefContext',
) -> List[Any]:
    # TODO kwargs not args
    own_attrs = OrderedDict()  # type: OrderedDict[str, Any]
    for stmt in ctx.cls.defs.body:
        if isinstance(stmt, AssignmentStmt):
            lvalue = stmt.lvalues[0]
            rvalue_ = stmt.rvalue
            attribute = get_native_typed_attribute(ctx, lvalue, rvalue_, stmt)
            own_attrs[attribute.name] = attribute

    for attribute in own_attrs.values():
        if attribute.name in ctx.cls.info.names:
            node = ctx.cls.info.names[attribute.name].node
            assert isinstance(node, Var)
            node.is_initialized_in_class = False

    attributes = list(own_attrs.values())

    return attributes


def _add_init(
        ctx: 'mypy.plugin.ClassDefContext',
        attributes: List[Attribute],
        adder: 'MethodAdder',
) -> None:
    """Generate an __init__ method for the attributes and add it to the class."""
    adder.add_method(
        '__init__',
        [attribute.argument(ctx) for attribute in attributes],
        NoneTyp()
    )


class MethodAdder:
    def __init__(self, ctx: 'mypy.plugin.ClassDefContext') -> None:
        self.ctx = ctx
        self.self_type = fill_typevars(ctx.cls.info)

    def add_method(
            self,
            method_name: str,
            args: List[Argument],
            ret_type: Type,
            self_type: Optional[Type] = None,
            tvd: Optional[TypeVarDef] = None,
    ) -> None:
        self_type = self_type if self_type is not None else self.self_type
        add_method(self.ctx, method_name, args, ret_type, self_type, tvd)


def value_object_class_callback(ctx: 'mypy.plugin.ClassDefContext') -> None:
    attributes = _analyze_class(ctx)
    adder = MethodAdder(ctx)
    _add_init(ctx, attributes, adder)


class CustomPlugin(Plugin):

    def get_base_class_hook(self, fullname: str):
        if 'cian_entities.base.ValueObject' == fullname:
            return value_object_class_callback


def plugin(*args, **kwargs):
    return CustomPlugin
